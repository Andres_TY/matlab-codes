clc
clear all
%% Values
Vf = 10
f=10000
C = 250e-9
R = 100
%% Zs definitions
Xc = -j*(1/(2*pi*f*C))
z= R+Xc
I = Vf/z
%% current
Im = abs(I)
Ia = angle(I)
%% voltage
Vr =  R*I
Vrm = abs(Vr)
Vra = angle(Vr)

Vc=  Xc*I
Vcm = abs(Vc)
Vca = angle(Vc)

%% Potencial
Sf = (Vf/sqrt(2))*(conj(I)/sqrt(2))
Sr = (Vr/sqrt(2))*(conj(I)/sqrt(2))
Sc = (Vc/sqrt(2))*(conj(I)/sqrt(2))

%% Balance
Sg = Sf
Sc = Sr+Sc
if Sg - Sc<=1e-20
    disp("Balanceado");
else
    disp("No balanceado");
end

%% FP

FP = cos(angle(Sg))
FP2 = (abs(Sr))/(abs(Sg))