clear all
clc

Vf1 = 10
Vf2 = 5
f = 10000
R1 = 30
R2 = 50
R3 = 10
C = 300e-9
L = 1e-3

w = 2*pi*f
%% Zs definitions

z1 = R1
z2 = R2
z3 = (-j)/(w*C)
z4 = j*w*L
z5 = R3

%%mesh
Zs = [(z1+z2),-z2,0;-z2,(z2+z3),-z3;0,z3,-(z3+z4+z5)]

V = [Vf1-Vf2;Vf2;0]

I = inv(Zs)*V
I4 = I(1)-I(2)
I5 = I(2)-I(3)

Vr1 = z1 * I(1)
Vr2 = z2 * I4
Vc = z3 * I5
Vl = z4 * I(3)
Vr3 = z5 * I(3)

%% balance 

sf1 = (Vf1/sqrt(2))*(conj(I(1))/sqrt(2))
sf2 = (Vf2/sqrt(2))*(conj(I4)/sqrt(2))
sr1 = (Vr1/sqrt(2))*(conj(I(1))/sqrt(2))
sr2 = (Vr2/sqrt(2))*(conj(I4)/sqrt(2))
sc = (Vc/sqrt(2))*(conj(I5)/sqrt(2))
sl = (Vl/sqrt(2))*(conj(I(3))/sqrt(2))
sr3 = (Vr3/sqrt(2))*(conj(I(3))/sqrt(2))

Sgen = sf1
Scon = sf2+sr1+sr2+sc+sl+sr3

%% FP

FP = cos(angle(Sgen))
FP2 = (abs(sr1+sr2+sr3))/(abs(Sgen))