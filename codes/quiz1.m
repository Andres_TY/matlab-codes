Vf =20
f = 1000
R1 = 500
R2 = 500
L = 40e-3
w = 2*pi*f
%% Zs definitions

z1 = R1
z2=R2
z3 = j*w*L
z4 = z2+z3
zeq = (z1*z4)/(z1+z4)
%% Current

I = Vf/zeq
I1 = Vf/z1
I2 = Vf/z4

%% Voltage

Vr1 = z1*I1
Vr2 = z2*I2
Vl = z3*I2

%% balance

sf = (Vf/sqrt(2))*(conj(I)/sqrt(2))
sr1 = (Vr1/sqrt(2))*(conj(I1)/sqrt(2))
sr2 = (Vr2/sqrt(2))*(conj(I2)/sqrt(2))
sl = (Vl/sqrt(2))*(conj(I2)/sqrt(2))

sg = sf
sc = sr1+sr2+sl

%% FP

FP = cos(angle(sg))
FP2 = (abs(sr1+sr2))/(abs(sg))