clc
clear all

%% Define

Vp = 50
f = 2500
L = 5e-3
C = 10e-6
R1=3
R2 = 10

%% Zs Definitios

z1 = j*(2*pi*f)*L
z2 = R1
z3 = 1/(j*(2*pi*f)*C)
z4 = R2

%% first reduction

z5 = z2+z3

%% second reduction

zeq = 1/((1/z1)+(1/z5)+(1/z4))

%% ohm laws

If = Vp/zeq

I1 = Vp/z1
I2=Vp/z5
I3 = Vp/z4

%% Voltage

Vl = z1*I1
Vr1 = z2*I2
Vc = z3*I2
Vr2 = z4*I3

%power balance

Sf = (Vp/sqrt(2)) * (conj(If)/sqrt(2))
Sl = (Vl/sqrt(2)) * (conj(I1)/sqrt(2))
Sr1 = (Vr1/sqrt(2)) * (conj(I2)/sqrt(2))
Sc = (Vc/sqrt(2)) * (conj(I2)/sqrt(2))
Sr2 = (Vr2/sqrt(2)) * (conj(I3)/sqrt(2))

Sgenerated = Sf
Sconsumed = Sl+Sr1+Sr2+Sc

if Sgenerated - Sconsumed <1e-8
    disp("Same");
else
    disp("Bad");
end

%% FP

FP = cos(angle(Sgenerated))
FP2 = (abs(Sr1+Sr2))/(abs(Sgenerated))