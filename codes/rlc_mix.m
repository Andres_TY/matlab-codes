clear all
clc

Vf = 10
f=10000
L = 1e-3
C = 300e-9

R1 = 3
R2 = 5
Xl = j*(2*pi*f*L)
Xc = 1/(2*pi*f*C*j)

%define Zs
z1 = R1
z2 = Xc
z3 = Xl
z4 = R2

%mesh
z = [z1+z2,-z2;-z2,z2+z3+z4]
V = [Vf;0]

I = inv(z)*V

I1m = abs(I(1))
I1a = angle(I(1))

I2m = abs(I(2))
I2a = angle(I(2))

%Voltaje

Vr1 = z1*I(1)
Vr1m = abs(Vr1)
Vr1a = angle(Vr1)

I3 = -I(2)+I(1)
Vc = z2 * (I3)
Vcm = abs(Vc)
Vca = angle(Vc)

Vl = z3 * I(2)
Vlm = abs(Vl)
Vla = angle(Vl)

Vr2 = z4*I(2)
Vr2m = abs(Vr2)
Vr2a = angle(Vr2)

%Validation

m1 = Vf-Vr1-Vc

m2 = -Vl - Vr2 + Vc

%Balance

sf = (Vf/sqrt(2))*(conj(I(1))/sqrt(2))
sr1 = (Vr1/sqrt(2))*(conj(I(1))/sqrt(2))
sc = (Vc/sqrt(2))*(conj(I3)/sqrt(2))
sr2 = (Vr2/sqrt(2))*(conj(I(2))/sqrt(2))
sl = (Vl/sqrt(2))*(conj(I(2))/sqrt(2))

Sgen = sf

Scons = sr1+sc+sr2+sl

%% FP

FP = cos(angle(Sgen))
FP2 = (abs(sr1+sr2))/(abs(Sgen))