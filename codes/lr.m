clc
clear all

%% Values
Vf = 10
f=60
L = 10e-3
R = 5

%% Zs definitions
Xl = j*(2*pi*f*L)
z= R+Xl
I = Vf/z

%% current
Im = abs(I)
Ia = angle(I)

%% voltaje
Vr =  R*I
Vrm = abs(Vr)
Vra = angle(Vr)

Vl=  Xl*I
Vlm = abs(Vc)
Vla = angle(Vc)

%% Potencial
Sf = (Vf/sqrt(2))*(conj(I)/sqrt(2))
Sr = (Vr/sqrt(2))*(conj(I)/sqrt(2))
Sl = (Vl/sqrt(2))*(conj(I)/sqrt(2))

%% Balance
Sg = Sf
Sc = Sr+Sl
if Sg - Sc<=1e-20
    disp("Balanceado");
else
    disp("No balanceado");
end

%% FP

FP = cos(angle(Sg))
FP2 = (abs(Sr))/(abs(Sg))