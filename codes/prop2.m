Vp1 = 12
Vp2 = 6
f=3000
R = 2
L = 1
C = 0.5

%% Zs definitions

z1 = R
z2 = j*(2*pi*f)*L
z3 = -j/(2*pi*f*C)

%% nodes
Vn2 = ((Vp1/z1)+(Vp2/z3))/((1/z1)+(1/z2)+(1/z3))

%% currents

I1 = (Vp1-Vn2)/z1
I2 = (Vp2 - Vn2)/z3
I3 = Vn2/z2

if abs(I1)<0
    I1=I1*(-1);
    disp("Change I1")
end

if abs(I2)<0
    I2=I2*(-1);
    disp("Change I2");
end

if abs(I3)<0
    I3=I3*(-1);
    disp("Change I3");
end
abs(I1)
angle(I1)
abs(I2)
angle(I2)
abs(I3)
angle(I3)



%% voltage

Vr = z1*I1
abs(Vr)
angle(Vr)
Vl = z2*I3
abs(Vl)
angle(Vl)
Vc = z3*I2
abs(Vc)
angle(Vc)

%%balance pow

sf1 = (Vp1/sqrt(2))*(conj(I1)/sqrt(2))
sf2 = (Vp2/sqrt(2))*(conj(I2)/sqrt(2))
sr = (Vr/sqrt(2))*(conj(I1)/sqrt(2))
sl = (Vl/sqrt(2))*(conj(I3)/sqrt(2))
sc = (Vc/sqrt(2))*(conj(I2)/sqrt(2))

Sgen = sf1+sf2
Scon = +sr+sl+sc

%% FP

FP = cos(angle(Sgen))
FP2 = (abs(sr))/(abs(Sgen))