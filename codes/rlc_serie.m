Vf = 10
f = 10000
R = 100
C = 300e-9
L = 1e-3

%redactance

Xl = j*(2*pi*f)*L
Xc = 1/(j*2*pi*f*C)

%define Zs

z1=R
z2=Xc
z3=Xl

%reduction

Zeq = z1+z2+z3

I = Vf/Zeq
Im = abs(I)
Ia = angle(I)
% Voltaje

Vr = R*I
Vrm = abs(Vr)
Vra = angle(Vr)

Vc = Xc*I
Vcm = abs(Vc)
Vca = angle(Vc)

Vl = Xl*I
Vlm = abs(Vl)
Vla = angle(Vl)

Vn1 = Vf
Vn2 = Vn1 - Vr
abs(Vn2)
angle(Vn2)
Vn3 = Vn2 - Vc

sf = (Vf/sqrt(2))*(conj(I)/sqrt(2))
sr = (Vr/sqrt(2))*(conj(I)/sqrt(2))
sc = (Vc/sqrt(2))*(conj(I)/sqrt(2))
sl = (Vl/sqrt(2))*(conj(I)/sqrt(2))

Sg = sf
Sc = sr+sc+sl

if Sg - Sc<=1e-20
    disp("Balanceado");
else
    disp("No balanceado");
end

%% FP

FP = cos(angle(Sg))
FP2 = (abs(sr))/(abs(Sg))