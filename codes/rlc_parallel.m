Vf = 10
f = 10000
R = 100
L =1e-3
C = 300e-9
%redactance

Xl = j*2*pi*f*L
Xc = 1/(j*2*pi*f*C)

%define Zs

z1 = R
z2 = Xl
z3 = Xc

%Zeq 

Zeq = 1/((1/z1)+(1/z2)+(1/z3))

%Currents

I1 = Vf/z1
I1m = abs(I1)
I1a = angle(I1)

I2 = Vf/z2
I2m = abs(I2)
I2a = angle(I2)

I3 = Vf/z3
I3m = abs(I3)
I3a = angle(I3)

Ieq = Vf/Zeq
Ieqm = abs(Ieq)
Ieqa = angle(Ieq)


sf = (Vf/sqrt(2))*(conj(Ieq)/sqrt(2))
sr = (Vf/sqrt(2))*(conj(I1)/sqrt(2))
sc = (Vf/sqrt(2))*(conj(I3)/sqrt(2))
sl = (Vf/sqrt(2))*(conj(I2)/sqrt(2))

sg = sf
sc = sr+sc+sl

if sg - sc<=1e-20
    disp("Balanceado");
else
    disp("No balanceado");
end

%% FP

FP = cos(angle(sg))
FP2 = (abs(sr))/(abs(sg))